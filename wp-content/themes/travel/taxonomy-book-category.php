<?php get_header(); ?>

<?php

$options = get_option('ct_coo_options');
$sidebar_config = $options['archive_sidebar_config'];
$config =configLayout(of_get_option('single-layout',$sidebar_config));
$left_sidebar = $options['archive_sidebar_left'];
$right_sidebar = $options['archive_sidebar_right'];
global $ct_has_blog;
$ct_has_blog = true;
$image_title = ct_featured_img_title();

$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
$term_id = $current_term->term_id;
$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>
    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">
                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <h1 class="heading-text">
                            <?php  if($seo_title){
                                echo $seo_title;
                            }
                            else{
                                single_cat_title();
                            } ?>
                        </h1>
                        <div class="desception-category"><?php echo category_description(); ?></div>
                        <?php if(have_posts()) : ?>

                            <div class="blog-wrap blog-items-wrap">

                                <!-- OPEN .blog-items -->
                                <ul class="mini-items book-items clearfix" id="blogGrid">

                                    <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    $post_format = get_post_format($post->ID);
                                    if ( $post_format == "" ) {
                                        $post_format = 'standard';
                                    }
                                    ?>
                                    <li <?php post_class('brand-item format-'.$post_format); ?>>
                                        <div class="mini-book-item-wrap">
                                            <figure class="animated-overlay overlay-alt faqs-img">
                                                <a>
                                                    <?php echo the_post_thumbnail('img-books');?>
                                                </a>
                                            </figure>
                                            <div class="brand-details-wrap">
                                                <h3 itemprop="name headline" class="title text-left">
                                                    <a><?php echo the_title();?></a>
                                                </h3>
                                                <div itemprop="description" class="excerpt">
                                                    <?php echo ct_excerpt('50');?>
                                                </div>

                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <!-- CLOSE .blog-items -->
                                </ul>

                            </div>

                        <?php else: ?>
                            <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>
                        <?php endif; ?>

                        <div class="pagination-wrap">
                            <?php echo pagenavi($wp_query); ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>