<?php get_header(); ?>

<?php

$options = get_option('ct_coo_options');

$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
$term_id = $current_term->term_id;

$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
$image_category  = category_image_src( array('size' =>'img-title') , false );

?>
<?php if($image_category){
    ?>
    <div class="row">
        <div class="title-image col-md-12">
            <img itemprop="image" src="<?php echo $image_category;?>" alt="img-culture" />
        </div>
    </div>
<?php
}?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>
    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page coo-main col-xs-12 col-sm-6 col-md-push-3 col-md-6 clearfix">
                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <h1 class="heading-cuisine">
                            <?php  if($seo_title){
                                echo $seo_title;
                            }
                            else{
                                single_cat_title();
                            } ?>
                        </h1>
                        <div class="desception-category desception-category-culture"><?php echo category_description(); ?></div>
                        <?php
                        $child_terms = get_term_children( $term_id, 'destination-category' );
                        if(!empty( $child_terms )){
                            echo "";
                    //        echo "</ul>\n";
//                            foreach ( $child_term//s as $child_term_id ) :
//                                $child_term = get_term_by( 'id', $child_term_id, 'd//estination-category' );
//                                echo '<li class="child"><a href="' . get_term_link( $child_term, 'destination-category' ) . '">' . $child_term//->name . "</a></li>\n";
                //            endforeach;
//                            echo "</ul>\n";
                        }
                        else{
                            if( have_posts() ): ?>
                            <?php query_posts($query_string . '&orderby=date&order=desc & posts_per_page=20 & parent=0' ); ?>
                            <div class="blog-wrap blog-items-wrap">
                                <div class="title-related"><span>A voir aussi :</span></div>
                                <!-- OPEN .blog-items -->
                                <ul class="mini-items clearfix">
                                    <?php $i=0;?>
                                    <?php while (have_posts()) : the_post(); ?>
                                        <?php
                                        $thumb_image = get_post_thumbnail_id();
                                        $item_title = get_the_title();
                                        $thumb_img_url = wp_get_attachment_url( $thumb_image, 'full' );
                                        $image = aq_resize( $thumb_img_url, 300, 225, true, false);
                                        ?>
                                        <?php if($i<3){?>
                                            <li class="related-item col-md-4 clearfix">
                                                <figure class="animated-overlay overlay-alt img-destination">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <img itemprop="image" src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo $item_title; ?>" />
                                                    </a>
                                                </figure>
                                                <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo the_title(); ?></a></h5>
                                            </li>
                                        <?php }
                                        else{
                                            ?>
                                            <li class="related-item-list col-md-6 clearfix">
                                                <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php echo the_title(); ?></a></h5>
                                            </li>
                                        <?php
                                        }
                                        ?>


                                        <?php $i++;?>
                                    <?php endwhile; ?>

                                    <!-- CLOSE .blog-items -->
                                </ul>

                            </div>
                            <?php else: ?>
                            <?php endif; ?>

                        <?php }
                        ?>
                        <div class="culture-bottom clearfix">
                            <div class="pull-left LikeButton clearfix">
                                <!-- Facebook Button -->
                                <div class="FacebookButton">
                                    <div id="fb-root"></div>
                                    <script type="text/javascript">
                                        (function (d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) {
                                                return;
                                            }
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));
                                    </script>
                                    <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                         data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                                </div>
                                <!-- Twitter Button -->
                                <div class="TwitterButton">
                                    <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                       data-count="horizontal" data-via="" data-size="small">
                                    </a>
                                </div>
                                <!-- Google +1 Button -->
                                <div class="GooglePlusOneButton">
                                    <!-- Place this tag where you want the +1 button to render -->
                                    <div class="g-plusone" data-size="medium"
                                         data-href="<?php the_permalink(); ?>"></div>
                                    <!-- Place this render call where appropriate -->
                                    <script type="text/javascript">
                                        (function () {
                                            var po = document.createElement('script');
                                            po.type = 'text/javascript';
                                            po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(po, s);
                                        })();
                                    </script>

                                </div>
                                <!-- Pinterest Button -->
                                <div class="PinterestButton">
                                    <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                       data-pin-do="buttonPin" data-pin-config="beside">
                                        <img class="pinterest"
                                             src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                    </a>
                                    <script type="text/javascript">
                                        (function (d) {
                                            var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                            p.type = 'text/javascript';
                                            p.async = true;
                                            p.src = '//assets.pinterest.com/js/pinit.js';
                                            f.parentNode.insertBefore(p, f);
                                        }(document));
                                    </script>
                                </div>
                                <!-- Linkedin Button -->
                                <div class="LinkedinButton">
                                    <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                            data-counter="right"></script>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-md-pull-6 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_destination_left'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->

            <!--Start.Sidebar-Right-->

            <div class="right-sidebar col-xs-12 col-sm-3 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_introduce'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-right -->
        </div>
    </div>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>