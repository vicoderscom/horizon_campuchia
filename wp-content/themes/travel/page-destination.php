<?php
/**
 * Template Name: Tour Destination
 */
?>

<?php get_header(); ?>

<?php
$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);

$title_image_url=$destination_image="";
$image_title = esc_attr( get_the_title( get_post_thumbnail_id() ) );
$title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
$destination_image = rwmb_meta('ct_destination_image', 'type=image&size=full');
if (is_array($title_image)) {
    foreach ($title_image as $image) {
        $title_image_url = $image['url'];
        break;
    }
}
if (is_array($destination_image)) {
    foreach ($destination_image as $image) {
        $destination_image_url = $image['url'];
        break;
    }
}
$args = array(
    'orderby'           => 'id',
    'order'             => 'ASC',
    'parent'            =>0,
    'hide_empty'        => false,
);
$aboutus_categories         = get_terms('destination-category',$args);
$i=0;
?>
<?php if($title_image_url){
    ?>
    <div class="row">
        <div class="title-image col-md-12">
            <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
        </div>
    </div>
<?php
}?>

    <div class="row">
        <div class="page-heading col-sm-12 clearfix  <?php echo $page_title_bg; ?>">
            <?php
            // BREADCRUMBS
            if (!$remove_breadcrumbs) {
                echo ct_breadcrumbs();
            }
            ?>
        </div>
    </div>
    <div class="inner-page-wrap row clearfix">
        <?php if (have_posts()) : the_post(); ?>
            <!-- Start.Main -->
            <div class="page-content coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content-inner">
                    <h1 class="title-page"><?php the_title();?></h1>
                    <div class="content-page"><?php the_content(); ?></div>
                    <div class="destination-block clearfix">
                        <div class="destination-list pull-left">
                            <ul class="destination-list-ul">
                                <?php
                                    if(count($aboutus_categories) > 0)
                                    {
                                        foreach($aboutus_categories as $cate) {
                                            $attachment_id   = get_option('categoryimage_'.$cate->term_id);
                                            $image_src = wp_get_attachment_image_src($attachment_id, 'img-des-category')[0];
                                            ?>
                                                <li class="destination-name">
                                                        <a href="<?php echo get_term_link($cate); ?>"><?php echo $cate->name; ?></a>
                                                </li>
                                            <?php
                                        }
                                    }
                                    ?>
                            </ul>
                        </div>
                        <div class="destination-img pull-right">
                            <?php echo $image = '<img itemprop="image" src="'.$destination_image_url.'" alt="'.$image_title.'" />';?>
                        </div>
                    </div>
                    <ul class="content-page-category row">
                        <div class="text-des"><?php echo __('A voir aussi:',TEXT_DOMAIN)?></div>
                        <?php
                        if(count($aboutus_categories) > 0)
                        {
                            foreach($aboutus_categories as $cate) {
                                $attachment_id   = get_option('categoryimage_'.$cate->term_id);
                                $image_src = wp_get_attachment_image_src($attachment_id, 'img-introduce')[0];
                                if($i<6){
                                    ?>
                                    <li class="item-destination col-md-4">
                                        <div class="item-destination-inner">
                                            <div class="item-category-inner clearfix">
                                                <h3 class="no-margin"><a class="title-category" href="<?php echo get_term_link($cate); ?>"><?php echo $cate->name; ?></a></h3>
                                                <a href="<?php echo get_term_link($cate); ?>">
                                                    <img itemprop="image" class="img-responsive" src="<?php echo $image_src ;?>" alt="<?php echo $cate->name; ?>">
                                                </a>
                                            </div>
                                            <div class="description">
                                                <?php echo ct_custom_excerpt($cate->description,20);?>
                                            </div>
                                            <a href="<?php echo get_term_link($cate); ?>" class="icon-read-more">[ + ]</a>
                                        </div>
                                    </li>
                                <?php
                                }
                                $i++;
                            }
                        }
                        ?>
                    </ul>
                    <div class="category-bottom clearfix">
                        <div class="pull-right LikeButton clearfix">
                            <!-- Facebook Button -->
                            <div class="FacebookButton">
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) {
                                            return;
                                        }
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/en_US/all.js#appId=177111755694317&xfbml=1";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-like" data-send="false" data-width="200" data-show-faces="true"
                                     data-layout="button_count" data-href="<?php the_permalink(); ?>"></div>
                            </div>
                            <!-- Twitter Button -->
                            <div class="TwitterButton">
                                <a href="<?php the_permalink(); ?>" class="twitter-share-button"
                                   data-count="horizontal" data-via="" data-size="small">
                                </a>
                            </div>
                            <!-- Google +1 Button -->
                            <div class="GooglePlusOneButton">
                                <!-- Place this tag where you want the +1 button to render -->
                                <div class="g-plusone" data-size="medium"
                                     data-href="<?php the_permalink(); ?>"></div>
                                <!-- Place this render call where appropriate -->
                                <script type="text/javascript">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://apis.google.com/js/plusone.js';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();
                                </script>

                            </div>
                            <!-- Pinterest Button -->
                            <div class="PinterestButton">
                                <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>&description=<?php the_title(); ?>"
                                   data-pin-do="buttonPin" data-pin-config="beside">
                                    <img class="pinterest"
                                         src="//assets.pinterest.com/images/pidgets/pin_it_button.png"/>
                                </a>
                                <script type="text/javascript">
                                    (function (d) {
                                        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
                                        p.type = 'text/javascript';
                                        p.async = true;
                                        p.src = '//assets.pinterest.com/js/pinit.js';
                                        f.parentNode.insertBefore(p, f);
                                    }(document));
                                </script>
                            </div>
                            <!-- Linkedin Button -->
                            <div class="LinkedinButton">
                                <script type="IN/Share" data-url="<?php the_permalink(); ?>"
                                        data-counter="right"></script>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End.Main -->
        <?php endif; ?>

        <div class="left-sidebar  col-xs-12 col-sm-3 col-sm-pull-9 col-md-3 ">
            <div class="sidebar-inner">
                <?php dynamic_sidebar('sidebar_destination'); ?>
            </div>
        </div>

        <!--End.Sidebar-right -->
    </div>



    <!--// WordPress Hook //-->
<?php get_footer(); ?>