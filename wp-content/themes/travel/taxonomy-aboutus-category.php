<?php get_header(); ?>

<?php

$options = get_option('ct_coo_options');
$sidebar_config = $options['archive_sidebar_config'];
$config =configLayout(of_get_option('single-layout',$sidebar_config));
$left_sidebar = $options['archive_sidebar_left'];
$right_sidebar = $options['archive_sidebar_right'];
global $ct_has_blog;
$ct_has_blog = true;
$image_title = ct_featured_img_title();
$image_category  = category_image_src( array('size' =>'img-title') , false );

$term_slug = get_query_var( 'term' );
$taxonomyName = get_query_var( 'taxonomy' );
$current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
$term_id = $current_term->term_id;
$cat_data = get_option("tag_$term_id");
$seo_title=$cat_data['seo_met_title'];
?>
    <?php if($image_category){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <img itemprop="image" src="<?php echo $image_category;?>" alt="img-culture" />
            </div>
        </div>
    <?php
    }?>
    <div class="row">
        <div class="page-heading col-md-12">
            <?php
            echo ct_breadcrumbs();
            ?>
        </div>
    </div>

    <div class="inner-page-wrap clearfix">
        <div class="row">
            <!-- Start.Main -->
            <div class="archive-page  coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix">

                <div class="page-content clearfix">
                    <div class="page-content-inner">
                        <h1 class="heading-text">
                            <?php  if($seo_title){
                                echo $seo_title;
                            }

                            else{
                                single_cat_title();
                            } ?>
                        </h1>
                        <div class="desception-category hidden"><?php echo category_description(); ?></div>
                        <?php if(have_posts()) : ?>
                            <?php query_posts($query_string . '&orderby=date&order=ASC'); ?>
                            <div class="blog-wrap blog-items-wrap">

                                <!-- OPEN .blog-items -->
                                <ul class="blog-items mini-items about-us-items clearfix" id="blogGrid">

                                    <?php while (have_posts()) : the_post(); ?>

                                    <?php
                                    $add_class='';
                                    $user_name = get_post_meta($post->ID, 'ct_user_name', true);
                                    $show_content = get_post_meta($post->ID, 'ct_show_content_post', true);
                                    if($show_content == "1"){
                                        $add_class='show-content';
                                    }
                                    $post_format = get_post_format($post->ID);
                                    if ( $post_format == "" ) {
                                        $post_format = 'standard';
                                    }
                                    ?>
                                    <li <?php post_class('about-us-item  format-'.$post_format); ?>>
                                        <div class="mini-about-us-wrap <?php echo $add_class ;?> clearfix">
                                            <figure class="animated-overlay overlay-alt  about-us-img">
                                                <a class="user-info" href="<?php echo get_permalink(); ?>">
                                                    <?php echo the_post_thumbnail('img-user');?>
                                                    <?php if($user_name){?>
                                                        <span class="user-name"><?php echo $user_name;?></span>
                                                    <?php }?>
                                                </a>
                                            </figure>
                                            <div class="about-us-details-wrap">
                                                <h3 itemprop="name headline" class="title">
                                                    <a href="<?php echo get_permalink(); ?>"><?php echo the_title();?></a>
                                                </h3>
                                                <div itemprop="description" class="excerpt">

                                                    <?php
                                                        if($show_content == "1"){
                                                            the_content();
                                                        }
                                                        else{

                                                            // echo ct_excerpt('200');
                                                            the_excerpt();
                                                        }

                                                    ?>

                                                </div>

                                            </div>
                                        </div>

                                        <?php endwhile; ?>

                                        <!-- CLOSE .blog-items -->
                                </ul>

                            </div>

                        <?php else: ?>
                            <h3><?php _e("Sorry, there are no posts to display.", "cootheme"); ?></h3>
                        <?php endif; ?>

                        <div class="pagination-wrap">
                            <?php echo pagenavi($wp_query); ?>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End.Main -->

            <!--Start.Sidebar-Left-->

            <div class="left-sidebar col-xs-12 col-sm-3 col-sm-pull-9 col-md-3">
                <div class="sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_aboutus'); ?>
                    <?php } ?>
                </div>
            </div>

            <!--End.Sidebar-Left -->
        </div>
    </div>


    <!--// WordPress Hook //-->
<?php get_footer(); ?>