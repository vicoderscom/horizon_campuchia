<?php /* Template Name: promotion page */ ?>

<?php get_header(); ?>

<?php
$remove_breadcrumbs = get_post_meta($post->ID, 'ct_no_breadcrumbs', true);

    $title_image_url="";
    $title_image = rwmb_meta('ct_title_image', 'type=image&size=img-title');
    if (is_array($title_image)) {
        foreach ($title_image as $image) {
            $title_image_url = $image['url'];
            break;
        }
    }
?>
    <?php if($title_image_url){
        ?>
        <div class="row">
            <div class="title-image col-md-12">
                <?php echo $image = '<img itemprop="image" src="'.$title_image_url.'" alt="'.$image_title.'" />';?>
            </div>
        </div>
    <?php
    }?>

    <div class="row">
        <div class="page-heading col-sm-12 clearfix  <?php echo $page_title_bg; ?>">
            <?php
            // BREADCRUMBS
            if (!$remove_breadcrumbs) {
                echo ct_breadcrumbs();
            }
            ?>
        </div>
    </div>
    <div class="inner-page-wrap row clearfix">
        <?php if (have_posts()) : the_post(); ?>
            <!-- Start.Main -->
            <div class="page-content coo-main col-xs-12 col-sm-9 col-sm-push-3 col-md-9 clearfix z-page">
                <div class="z-head">
                    <div class="col-xs-12 z-left">
                        <p>
                            Remise de 5 % sur nos<br>
                            tarifs pour tous nos<br>
                            anciens clients
                        </p>
                    </div>
                	<div class="col-xs-12 z-center"></div>
                    <div class="col-xs-12 z-right">
                        <p>
                            Pour vos amis 5 % de<br>
                            remise immédiate sur<br>
                            leur premier voyage
                        </p>
                    </div>
                </div>
                <div class="z-r-one row">
                	<p>Jusqu’à 130 € de crédit offert pour tout parrainage</p>
                	<a href="<?php echo esc_url( get_permalink(6829) ); ?>">Voir le détail</a>
                </div>
                <div class="z-section-3 row">
                    <div class="col-xs-12 col-sm-6 z-3-left">
                        <div class="col-xs-12 col-sm-7 img">
                            <a href="<?php echo get_site_url(); ?>/nos-temoignages/nos-voyageurs-parlent/"><img src="/wp-content/themes/travel/images/z-page/list-human.png" class="img-responsive"></a>
                        </div>
                        <div class="col-xs-12 col-sm-5 text">
                            Nos<br>
                            témoignages
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 z-3-right">
                        <div class="text col-xs-12 col-sm-8">
                            <p>
                                Pour tous renseignements<br>
                                et inscriptions
                            </p>
                            <a href="<?php echo get_site_url(); ?>/contact/">Contactez un<br>conseiller</a>
                        </div>
                    </div>
                </div>
                <div class="z-section-4 row">
                    <div class="col-xs-12 col-sm-6 z-4-left">
                        <h3>10 RAISONS POUR VOYAGER AVEC NOUS</h3>
                        <ul>
                            <li>Agence locate sérieuse/prix direct</li>
                            <li>Un voyage personnalisé et privé à 100%</li>
                            <li>Assistance locale 24h/24</li>
                            <li>Un prix le plus économique du marché</li>
                            <li>Sécurité et sérénité</li>
                            <li>Garantie de satisfaction </li>
                        </ul>
                        <a href="<?php echo get_site_url(); ?>/notre-mission/pourquoi-nous/" class="voir-plus"><i class="fa fa-forward" aria-hidden="true"></i> Voir plus</a>
                        <div class="z-ghim">
                            <img src="/wp-content/themes/travel/images/z-page/ghim.png">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 z-4-right">
                        <div class="z-title">
                            <h3>Voyage Solidaire</h3>
                        </div>
                        <a href="<?php echo get_site_url(); ?>/voyage-responsable-horizon-vietnam/agence-locale-vietnam/"><img src="/wp-content/themes/travel/images/z-page/children.png" class="img-responsive"></a>
                    </div>
                </div>
                <div class="z-section-5 row">
                    <div class="z-column col-xs-6 col-sm-3">
                        <a href="<?php echo get_site_url(); ?>/agence-voyage-vietnam/"><img src="/wp-content/themes/travel/images/z-page/51.png" class="img-responsive"></a>
                        <h3>QUI SOMMES NOUS?</h3>
                    </div>
                    <div class="z-column col-xs-6 col-sm-3">
                        <a href="<?php echo get_site_url(); ?>/agence-locale-vietnam-recommandee/guides-parlent-de-nous/"><img src="/wp-content/themes/travel/images/z-page/52.png" class="img-responsive"></a>
                        <h3>LES QUIDES NOUS RECOMMANDENT</h3>
                    </div>
                    <div class="z-column col-xs-6 col-sm-3">
                        <a href="<?php echo get_site_url(); ?>/pourquoi-une-agence-locale/"><img src="/wp-content/themes/travel/images/z-page/53.png" class="img-responsive"></a>
                        <h3>POURQUOI UNE AGENCE LOCALE?</h3>
                    </div>
                    <div class="z-column col-xs-6 col-sm-3">
                        <a href="<?php echo get_site_url(); ?>/nos-temoignages/nos-voyageurs-parlent/"><img src="/wp-content/themes/travel/images/z-page/54.png" class="img-responsive"></a>
                        <h3>NOS TÉMOIGNAGES</h3>
                    </div>
                </div>
            </div>
            <!-- End.Main -->
        <?php endif; ?>

        <div class="left-sidebar  col-xs-12 col-sm-3 col-sm-pull-9 col-md-3 ">
            <div class="sidebar-inner">
                <?php dynamic_sidebar('sidebar_aboutus'); ?>
            </div>
        </div>

             <!--End.Sidebar-right -->
    </div>



<!--// WordPress Hook //-->
<?php get_footer(); ?>