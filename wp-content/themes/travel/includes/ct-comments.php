<?php
	
	/*
	*
	*	Cootheme Comments Functions
	*	------------------------------------------------
	*	Cootheme v2.0
	* 	http://www.cootheme.com
	*
	*	ct_custom_comments()
	*	custom_pings()
	*
	*/
	
	/* COMMENTS
	================================================== */
	if (!function_exists('ct_custom_comments')) {
		function ct_custom_comments($comment, $args, $depth) {
		  $GLOBALS['comment'] = $comment;
		    $GLOBALS['comment_depth'] = $depth;
		  ?>
		    <li id="comment-<?php comment_ID() ?>" <?php comment_class('clearfix') ?>>
		        <div class="comment-wrap media clearfix">
		            <div class="comment-avatar media-left">
		            	<?php if(function_exists('get_avatar')) { echo get_avatar($comment, '80'); } ?>
		            	<?php if ($comment->comment_author_email == get_the_author_meta('email')) { ?>
		            	<span class="tooltip"><?php _e("Author", "cootheme"); ?><span class="arrow"></span></span>
		            	<?php } ?>
		            </div>
		    		<div class="comment-content  media-body">
		            	<div class="comment-meta">
	            			<?php
	            				printf('<h4 class="comment-author media-heading">%1$s</h4>',
	            					get_comment_author_link(),
	            					human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ' . __("ago", "cootheme")
	            				);
	            			?>
			            	<div class="comment-meta-actions">
			            		<?php
		            				printf('<span class="comment-date">%2$s</span>',
		            					get_comment_author_link(),
		            					human_time_diff( get_comment_time('U'), current_time('timestamp') ) . ' ' . __("ago", "cootheme")
		            				);
		            			?>
		            			<span><?php the_date(); ?> /</span>
		            			<?php
		                        	edit_comment_link(__('Edit', 'cootheme'), '<span class="edit-link">', ' /</span>');
		                        ?>
		                        <?php if($args['type'] == 'all' || get_comment_type() == 'comment') :
		                        	comment_reply_link(array_merge($args, array(
		                            	'reply_text' => __('Reply','cootheme'),
		                            	'login_text' => __('Log in to reply.','cootheme'),
		                            	'depth' => $depth,
		                            	'before' => '<span class="comment-reply">',
		                            	'after' => '</span>'
		                        	)));
		                        endif; ?>
			                </div>
						</div>
		      			<?php if ($comment->comment_approved == '0') _e("\t\t\t\t\t<span class='unapproved'>Your comment is awaiting moderation.</span>\n", 'cootheme') ?>
		            	<div class="comment-body">
		                	<?php comment_text() ?>
		            	</div>
		    		</div>
		        </div>
	<?php }
	} // end ct_custom_comments
	
	// Custom callback to list pings
	function custom_pings($comment, $args, $depth) {
	       $GLOBALS['comment'] = $comment;
	        ?>
	            <li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
	                <div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'cootheme'),
	                        get_comment_author_link(),
	                        get_comment_date(),
	                        get_comment_time() );
	                        edit_comment_link(__('Edit', 'cootheme'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
	    <?php if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'cootheme') ?>
	            <div class="comment-content">
	                <?php comment_text() ?>
	            </div>
	<?php } // end custom_pings
	
?>