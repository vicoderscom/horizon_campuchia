<?php
	$options = get_option('ct_coo_options');

	$enable_footer = $options['enable_footer'];
	$enable_footer_divider = $options['enable_footer_divider'];
	$enable_copyright = $options['enable_copyright'];
	$enable_copyright_divider = $options['enable_copyright_divider'];
	$show_backlink = $options['show_backlink'];

	$footer_config = $options['footer_layout'];
	$copyright_text = __($options['footer_copyright_text'], 'cootheme');

	
	global $post, $ct_include_infscroll;
	$remove_promo_bar = false;
	if ($post) {
	$remove_promo_bar = get_post_meta($post->ID, 'ct_remove_promo_bar', true);
	}
	
	$footer_class = $copyright_class = "";
	
	if ($enable_footer_divider) { $footer_class = "footer-divider"; }
	if ($enable_copyright_divider) { $copyright_class = "copyright-divider"; }

    // $args = [
    //     'post_type' => 'security',
    //     'posts_per_page' => 4,
    //     'post_status' => ['publish'],
    //     'order' => 'ASC',
    //     'orderby'=>'ID',
    //     'tax_query' => array(
    //         array(
    //             'taxonomy' => 'security-category',
    //             'field' => 'id',
    //             'terms' => 974
    //         )
    //     )
    // ];
    // $security_category = get_category('974');
    // if(empty($security_category->slug)) {
    //     $security_category->slug = 'nos-security';
    // }
    // $custom_posts = new WP_Query($args);
    // echo "<pre>";
    // var_dump($security_category); die;
?>

			<div id="footer-wrap">
			    <div class="container-full security-container">
                    <div class="row clearfix security-row">                        
                        <?php 
                            if(is_active_sidebar('security-footer-1')) {
                                echo '<div class="col-xs-12 col-sm-6 col-md-3 security-sub-row">';
                                dynamic_sidebar('security-footer-1');
                                echo '</div>';
                            } 
                            if(is_active_sidebar('security-footer-2')) {
                                echo '<div class="col-xs-12 col-sm-6 col-md-3 security-sub-row">';
                                dynamic_sidebar('security-footer-2');
                                echo '</div>';
                            }
                            if(is_active_sidebar('security-footer-3')) {
                                echo '<div class="col-xs-12 col-sm-6 col-md-3 security-sub-row">';
                                dynamic_sidebar('security-footer-3');
                                echo '</div>';
                            }
                            if(is_active_sidebar('security-footer-4')) {
                                echo '<div class="col-xs-12 col-sm-6 col-md-3 security-sub-row">';
                                dynamic_sidebar('security-footer-4');
                                echo '</div>';
                            }
                        ?>
                    </div>
                </div>
                <?php if ($enable_footer) { ?>

                <!--// OPEN #footer //-->
                <section id="footer" class="<?php echo $footer_class; ?>">
                    <div class="footer-inner">
                        <div class="container-full">
                            <div id="footer-widgets" class="row clearfix">
                                <?php if ($footer_config == "footer-1") { ?>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('footer-column-1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 3'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 4'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-2") { ?>

                                <div class="col-sm-12 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 3'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-3") { ?>

                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 3'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-4") { ?>

                                <div class="col-sm-12 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-5") { ?>

                                <div class="col-sm-12 col-md-4">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 3'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-6") { ?>

                                <div class="col-sm-12 col-md-4">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-8">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-7") { ?>

                                <div class="col-sm-12 col-md-8">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>

                                <?php } else if ($footer_config == "footer-8") { ?>

                                <div class="col-sm-6 col-md-3">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 2'); ?>
                                <?php } ?>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 3'); ?>
                                <?php } ?>
                                </div>

                                <?php } else { ?>

                                <div class="col-sm-12 col-md-12">
                                <?php if ( function_exists('dynamic_sidebar') ) { ?>
                                    <?php dynamic_sidebar('Footer Column 1'); ?>
                                <?php } ?>

                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                <!--// CLOSE #footer //-->
                </section>
                <?php } ?>

                <?php
                    $cootheme_backlink = "";
                    if ($show_backlink) {
                    $cootheme_backlink =	apply_filters("cootheme_link", " <a href='http://www.rivertheme.com/'>Premium WordPress Themes by Cootheme</a>");
                    }

                if ($enable_copyright) { ?>
                <?php } ?>

                <?php if ($options['google_analytics'] != "") {
                    echo  "<div class='google_analytics'>".$options['google_analytics']."</div>";
                } ?>
                <div class="band-client row  clearfix">
                    <div class="col-md-12">
                        <?php if ( function_exists('dynamic_sidebar') ) { ?>
                            <?php dynamic_sidebar('band-client'); ?>
                        <?php } ?>
                    </div>
                </div>
                <!--// OPEN #copyright //-->
                <footer id="copyright" class="<?php echo $copyright_class; ?>">
                    <div class="container">
                        <div class="language-swicher">
                            <a class="lang-vi current" href="http://horizon-vietnamvoyage.com/">Vietnam</a>
                            <a class="lang-cambodge" href="http://cambodge.horizon-vietnamvoyage.com">Cambodge</a>
                            <a class="lang-lao" href="http://laos.horizon-vietnamvoyage.com/">Laos</a>

                            <a class="lang-myanmar" href="http://horizon-birmanievoyage.com/">Birmanie</a>
                            <a class="lang-chine" href="http://horizon-chinevoyage.com/">Chine</a>
                        </div>

                        <p>
                            <?php 
                                echo do_shortcode(stripslashes($copyright_text));
                            ?>
                            <?php 
                                // echo $cootheme_backlink; 
                            ?>
                        </p>

                        <nav class="footer-menu std-menu">
                            <?php
                                $footer_menu_args = array(
                                    'echo'            => true,
                                    'theme_location' => 'footer_menu',
                                    'fallback_cb' => ''
                                );
                                wp_nav_menu( $footer_menu_args );
                            ?>
                        </nav>
                    </div>
                <!--// CLOSE #copyright //-->
                </footer>



			
			</div>
            <div class="out_site_fly">
                <div class="out_site_fly_right sidebar-inner">
                    <?php if ( function_exists('dynamic_sidebar') ) { ?>
                        <?php dynamic_sidebar('sidebar_fly'); ?>
                    <?php } ?>
                </div>
            </div>
        <!--// CLOSE #page-wrap //-->
        </div>
    <!--// CLOSE #container //-->
    </div>
</div>

		
		<!--// FULL WIDTH VIDEO //-->
		<div class="fw-video-area"><div class="fw-video-close"><i class="ss-delete"></i></div></div><div class="fw-video-spacer"></div>
		
		<?php if ($ct_include_infscroll) { ?>
		<div id="inf-scroll-params" data-loadingimage="<?php echo get_template_directory_uri(); ?>/images/loader.gif" data-msgtext="<?php _e("Loading", "cootheme");
		?>" data-finishedmsg="<?php _e("All posts loaded", "cootheme"); ?>"></div>
		<?php } ?>
						
		<!--// FRAMEWORK INCLUDES //-->
		<div id="ct-included" class="<?php echo ct_global_include_classes(); ?>"></div>

			
		<!--// WORDPRESS FOOTER HOOK //-->
		<?php wp_footer(); ?>
    <!-- Google Code dành cho Thẻ tiếp thị lại -->
        <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 952371724;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/952371724/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
        </noscript>
	<!--// CLOSE BODY //-->

        <div class="back-to-top">
            <a href="javascript:void(0)">
                <i class="fa fa-chevron-up" aria-hidden="true"></i>
            </a>
        </div>
        <script type="text/javascript">
            $(window).on('scroll', function () {
                if ($(window).scrollTop() > 20) {
                    $(".back-to-top").css("display", "block");
                } else {
                    $(".back-to-top").css("display", "none");
                }
            });
            if($('.back-to-top').length){
                $(".back-to-top").on('click', function() {
                    $('html, body').animate({
                    scrollTop: $('html, body').offset().top
                    }, 1000);
                });
            }
        </script>

        <script>
            if(window.location.pathname.match('/booking-tour/')){ 
                formconv('booking-tour');
            } 
            if(window.location.pathname.match('/contact/')){ 
                formconv('contact');
            }
            if(window.location.pathname.match('/reminder/')){ 
                formconv('reminder');
            }

            function formconv(formName){
                jQuery('.wpcf7-submit').click(function(){
                var myVar = setInterval(function(){ var x = 0;  if(x==0){
                    if(jQuery('.wpcf7-mail-sent-ok').length>0){ 
                    ga('send', 'event','form','submit',formName);    
                    clearInterval(myVar); x=1;
                    }  }  }, 1000); 
                })
            } 
        </script>

	</body>


<!--// CLOSE HTML //-->
</html>