<?php

	/* ==================================================

	Books Post Type Functions

	================================================== */
	$args = array(
	    "label" 						=> _x('Responsable Category', 'category label', "coo-theme-admin"),
	    "singular_label" 				=> _x('Responsable', 'category singular label', "coo-theme-admin"),
	    'public'                        => true,
	    'hierarchical'                  => true,
	    'show_ui'                       => true,
	    'show_in_nav_menus'             => true,
	    'args'                          => array( 'orderby' => 'term_order' ),
        'rewrite'                       => array(
                                            'slug' => 'voyage-responsable-horizon-vietnam',
                                            'with_front' => false ),
	    'query_var'                     => true
	);

	register_taxonomy( 'responsibility-category', 'responsibility', $args );


	add_action('init', 'responsibility_register');

	function responsibility_register() {

	    $labels = array(
	        'name' => _x('Responsable', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Responsable', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'job', "coo-theme-admin"),
	        'add_new_item' => __('Add New Responsable', "coo-theme-admin"),
	        'edit_item' => __('Edit Responsable', "coo-theme-admin"),
	        'new_item' => __('New Responsable', "coo-theme-admin"),
	        'view_item' => __('View Responsable', "coo-theme-admin"),
	        'search_items' => __('Search Responsable', "coo-theme-admin"),
	        'not_found' =>  __('No Responsable have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => true,
	        'rewrite' =>  array('slug' => 'voyage-solidaire-horizon-vietnam','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        'taxonomies' => array('responsibility-category', 'post_tag',)
	       );

	    register_post_type( 'responsibility' , $args );
	}
	add_filter("manage_edit-responsibility_columns", "responsibility_edit_columns");
	function responsibility_edit_columns($columns){
	        $columns = array(
	            "cb" => "<input type=\"checkbox\" />",
	            "thumbnail" => "",
	            "title" => __("Responsable", "coo-theme-admin"),
	            "description" => __("Description", "coo-theme-admin"),
	            "responsibility-category" => __("Categories", "coo-theme-admin")
	        );

	        return $columns;
	}

?>