<?php

	/* ==================================================

	Livress Post Type Functions

	================================================== */


	// $args = array(
	//     "label" 						=> _x('Livres Categories', 'category label', "coo-theme-admin"),
	//     "singular_label" 				=> _x('Livres Category', 'category singular label', "coo-theme-admin"),
	//     'public'                        => true,
	//     'hierarchical'                  => true,
	//     'show_ui'                       => true,
	//     'show_in_nav_menus'             => true,
	//     'args'                          => array( 'orderby' => 'term_order' ),
 //        'rewrite'                       => array(
 //                                                'slug' => 'agence-locale-vietnam-recommandee',
 //                                                'with_front' => false ),
	//     'query_var'                     => true
	// );

	// register_taxonomy( 'book-category', 'book', $args );


	add_action('init', 'office_register');

	function office_register() {

	    $labels = array(
	        'name' => _x('Office', 'post type general name', "coo-theme-admin"),
	        'singular_name' => _x('Office', 'post type singular name', "coo-theme-admin"),
	        'add_new' => _x('Add New', 'Our Offices', "coo-theme-admin"),
	        'add_new_item' => __('Add New Office', "coo-theme-admin"),
	        'edit_item' => __('Edit Office', "coo-theme-admin"),
	        'new_item' => __('New Office', "coo-theme-admin"),
	        'view_item' => __('View Office', "coo-theme-admin"),
	        'search_items' => __('Search Offices', "coo-theme-admin"),
	        'not_found' =>  __('No Office have been added yet', "coo-theme-admin"),
	        'not_found_in_trash' => __('Nothing found in Trash', "coo-theme-admin"),
	        'parent_item_colon' => ''
	    );

	    $args = array(
	        'labels' => $labels,
	        'public' => true,
	        'show_ui' => true,
	        'show_in_menu' => true,
	        'show_in_nav_menus' => false,
            //'rewrite' =>  array('slug' => 'agence-vietnamienne-recommandee','with_front' => false ),
	        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
	        'has_archive' => true,
	        //'taxonomies' => array('book-category', 'post_tag')
	    );

	    register_post_type( 'office' , $args );
	}

	// add_filter("manage_edit-book_columns", "book_edit_columns");

	// function book_edit_columns($columns){
	//         $columns = array(
	//             "cb" => "<input type=\"checkbox\" />",
	//             "thumbnail" => "",
	//             "title" => __("Livres", "coo-theme-admin"),
	//             "description" => __("Description", "coo-theme-admin"),
	//             "book-category" => __("Categories", "coo-theme-admin")
	//         );

	//         return $columns;
	// }

?>