<?php

	/*
	*
	*	Meta Box Functions
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/
	
	$prefix = 'ct_';
	$text_domain = "coo-theme-admin";
	
	global $meta_boxes;
	
	$meta_boxes = array();
		
	$options = get_option('ct_coo_options');
	$default_page_heading_bg_alt = isset($options['default_page_heading_bg_alt'])?$options['default_page_heading_bg_alt']:null;
	$default_show_page_heading = isset($options['default_show_page_heading'])?$options['default_show_page_heading']:null;
	$default_sidebar_config = $options['default_sidebar_config'];
	$default_left_sidebar = $options['default_left_sidebar'];
	$default_right_sidebar = $options['default_right_sidebar'];
	
	if (!$default_page_heading_bg_alt || $default_page_heading_bg_alt == "") {
		$default_page_heading_bg_alt = "none";
	}
	if ($default_show_page_heading == "") {
		$default_show_page_heading = 1;
	}
	if ($default_sidebar_config == "") {
		$default_sidebar_config = "no-sidebars";
	}		
	if ($default_left_sidebar == "") {
		$default_left_sidebar = "Sidebar-1";
	}		
	if ($default_right_sidebar == "") {
		$default_right_sidebar = "Sidebar-1";
	}
	
	$default_product_sidebar_config = $default_product_left_sidebar = $default_product_right_sidebar = "";
	
	if (isset($options['default_product_sidebar_config'])) {
	$default_product_sidebar_config = $options['default_product_sidebar_config'];
	}
	if (isset($options['default_product_left_sidebar'])) {
	$default_product_left_sidebar = $options['default_product_left_sidebar'];
	}
	if (isset($options['default_product_right_sidebar'])) {
	$default_product_right_sidebar = $options['default_product_right_sidebar'];
	}
	
	if ($default_product_sidebar_config = "") {
		$default_product_sidebar_config = "no-sidebars";
	}
	if ($default_product_left_sidebar == "") {
		$default_product_left_sidebar = "Sidebar-1";
	}		
	if ($default_product_right_sidebar == "") {
		$default_product_right_sidebar = "Sidebar-1";
	}
	
	

    /* Image Tittle
    ================================================== */
    $meta_boxes[] = array(
        'id' => 'image_title_meta_box',
        'title' => __('Background Heade Options', $text_domain),
        'pages' => array( 'post', 'page' ),
        'context' => 'normal',
        'fields' => array(

            // BACKGROUND IMAGE
            array(
                'name'  => __('Background Header', $text_domain),
                'desc'  => __('The image that will be used as the title, default size: height=980px,width:246px', $text_domain),
                'id'    => "{$prefix}title_image",
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            )

        )
    );
    /* Post Meta Box Le Viet Nam
        ================================================== */
    $meta_boxes[] = array(
        'id' => 'post_meta_box',
        'title' => __('Post Meta', $text_domain),
        'pages' => array( 'levietnam','guide','book','special','responsibility','destination','sea'),
        'context' => 'normal',
        'fields' => array(
            // SHOW PAGE TITLE
            array(
                'name' => __('Seo title', $text_domain),
                'id' => $prefix . 'topbar_title',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Show page img', $text_domain),    // File type: checkbox
                'id'   => "{$prefix}page_img",
                'type' => 'checkbox',
                'desc' => __('Show the page image at the top of the page.', $text_domain),
                'std' => 0,
            ),
            // BACKGROUND IMAGE
            array(
                'name'  => __('Background Header', $text_domain),
                'desc'  => __('The image that will be used as the title, default size: height=980px,width:246px', $text_domain),
                'id'    => "{$prefix}title_image",
                'type'  => 'image_advanced',
                'max_file_uploads' => 1
            )
        )
    );
    /* Post Meta Box Le Viet Nam
            ================================================== */
    $meta_boxes[] = array(
        'id' => 'post_meta_box',
        'title' => __('Post Meta', $text_domain),
        'pages' => array( 'aboutus' ),
        'context' => 'normal',
        'fields' => array(

            // User Name
            array(
                'name' => __('User Name', $text_domain),
                'id' => $prefix . 'user_name',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            // SHOW content post category
            array(
                'name' => __('Show content post on the category', $text_domain),    // File type: checkbox
                'id'   => "{$prefix}show_content_post",
                'type' => 'checkbox',
                'desc' => __('Show content post on the category.', $text_domain),
                'std' => 0,
            ),
            array(
                'name' => __('Seo title', $text_domain),
                'id' => $prefix . 'topbar_title',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),

        )
    );
    $meta_boxes[] = array(
        'id' => 'post_topbar_box',
        'title' => __('Seo Meta', $text_domain),
        'pages' => array( 'post','sea' ),
        'context' => 'normal',
        'fields' => array(
            array(
                'name' => __('Seo title', $text_domain),
                'id' => $prefix . 'topbar_title',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),

        )
    );
                /* Post Meta Box
                ================================================== */
	$meta_boxes[] = array(
		'id' => 'post_meta_box',
		'title' => __('Tour Information', $text_domain),
		'pages' => array( 'post','sea'),
		'context' => 'normal',
		'fields' => array(

            array(
                'name' => __('Code:', $text_domain),
                'id' => $prefix . 'tour_code',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Time Tour:', $text_domain),
                'id' => $prefix . 'tour_time',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Time Tour court:', $text_domain),
                'id' => $prefix . 'tour_time_short',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Commencement Tour:', $text_domain),
                'id' => $prefix . 'tour_commencement',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Depart:', $text_domain),
                'id' => $prefix . 'tour_depart',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Duree:', $text_domain),
                'id' => $prefix . 'tour_duree',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Circuit:', $text_domain),
                'id' => $prefix . 'tour_circuit',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Départ:', $text_domain),
                'id' => $prefix . 'tour_departs',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
            array(
                'name' => __('Programme:', $text_domain),
                'id' => $prefix . 'tour_programme',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),

		)
	);
	
	

	
	
	/* Team Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id'    => 'team_meta_box',
		'title' => __('Team Member Meta', $text_domain),
		'pages' => array( 'team' ),
		'fields' => array(
		
			// TEAM MEMBER DETAILS SECTION
			array (
				'name' 	=> '',
				'title' => __('Team Member Details', $text_domain),
			    'id' 	=> "{$prefix}heading_team_member_details",
			    'type' 	=> 'section'
			),
			
			// TEAM MEMBER POSITION
			array(
				'name' => __('Position', $text_domain),
				'id' => $prefix . 'team_member_position',
				'desc' => __("Enter the team member's position within the team.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER EMAIL
			array(
				'name' => __('Email Address', $text_domain),
				'id' => $prefix . 'team_member_email',
				'desc' => __("Enter the team member's email address.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER PHONE NUMBER
			array(
				'name' => __('Phone Number', $text_domain),
				'id' => $prefix . 'team_member_phone_number',
				'desc' => __("Enter the team member's phone number.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER TWITTER
			array(
				'name' => __('Twitter', $text_domain),
				'id' => $prefix . 'team_member_twitter',
				'desc' => __("Enter the team member's Twitter username.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER FACEBOOK
			array(
				'name' => __('Facebook', $text_domain),
				'id' => $prefix . 'team_member_facebook',
				'desc' => __("Enter the team member's Facebook URL.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER LINKEDIN
			array(
				'name' => __('LinkedIn', $text_domain),
				'id' => $prefix . 'team_member_linkedin',
				'desc' => __("Enter the team member's LinkedIn URL.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER GOOGLE+
			array(
				'name' => __('Google+', $text_domain),
				'id' => $prefix . 'team_member_google_plus',
				'desc' => __("Enter the team member's Google+ URL.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER SKYPE
			array(
				'name' => __('Skype', $text_domain),
				'id' => $prefix . 'team_member_skype',
				'desc' => __("Enter the team member's Skype username.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER INSTAGRAM
			array(
				'name' => __('Instagram', $text_domain),
				'id' => $prefix . 'team_member_instagram',
				'desc' => __("Enter the team member's Instragram URL (e.g. http://hashgr.am/).", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			),
			
			// TEAM MEMBER DRIBBBLE
			array(
				'name' => __('Dribbble', $text_domain),
				'id' => $prefix . 'team_member_dribbble',
				'desc' => __("Enter the team member's Dribbble username.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => '',
			)
		)
	);
	
	
	/* Clients Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id'    => 'client_meta_box',
		'title' => __('Client Meta', $text_domain),
		'pages' => array( 'clients' ),
		'fields' => array(
			
			// CLIENT IMAGE LINK
			array(
				'name' => __('Client Link', $text_domain),
				'id' => $prefix . 'client_link',
				'desc' => __("Enter the link for the client if you want the image to be clickable.", $text_domain),
				'clone' => false,
				'type'  => 'text',
				'std' => ''
			)
		)	
	);
	
	
	/* Testimonials Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id'    => 'testimonials_meta_box',
		'title' => __('Testimonial Meta', $text_domain),
		'pages' => array( 'testimonials' ),
		'fields' => array(
			

			array(
				'name' => __('Testimonial Name', $text_domain),
				'id' => $prefix . 'testimonial_name',
				'desc' => __("Enter your name.", $text_domain),
				'type'  => 'text',
				'std' => ''
			),

			array(
				'name' => __('Testimonial Number', $text_domain),
				'id' => $prefix . 'testimonial_number',
				'desc' => __("Enter group personnes.", $text_domain),
				'type'  => 'text',
				'std' => ''
			),
            array(
                'name' => __('Testimonial Address', $text_domain),
                'id' => $prefix . 'testimonial_address',
                'desc' => __("Enter Address.", $text_domain),
                'type'  => 'text',
                'std' => ''
            ),
            array(
                'name' => __('Testimonial Tour Name', $text_domain),
                'id' => $prefix . 'testimonial_tour_name',
                'desc' => __("Enter Tour.", $text_domain),
                'type'  => 'text',
                'std' => ''
            ),
            array(
                'name' => __('Testimonial  Tour Type', $text_domain),
                'id' => $prefix . 'testimonial_tour_type',
                'desc' => __("Enter Tour Type.", $text_domain),
                'type'  => 'text',
                'std' => ''
            ),
            array(
                'name' => __( 'Datetime', $text_domain ),
                'id'   => $prefix . 'testimonial_datetime',
                'desc' => __("Enter Date time.", $text_domain),
                'type' => 'datetime',
            ),
            array(
                'name' => __( 'Email', $text_domain ),
                'id'   => $prefix . 'testimonial_email',
                'desc' => __("Enter email.", $text_domain),
                'type' => 'text',
            ),
			// TESTIMONAIL IMAGE
			array(
				'name'  => __('Testimonial  Image', $text_domain),
				'desc'  => __('Enter the cite image for the testimonial (optional).', $text_domain),
				'id'    => "{$prefix}testimonial_cite_image",
				'type'  => 'image_advanced',
				'max_file_uploads' => 1
			),
            // SHOW PAGE TITLE
            array(
                'name' => __('Seo title', $text_domain),
                'id' => $prefix . 'topbar_title',
                'clone' => false,
                'type'  => 'text',
                'std' => '',
            ),
		)	
	);
/* Embassies Meta Box
================================================== */
$meta_boxes[] = array(
    'id'    => 'embassies_meta_box',
    'title' => __('Embassies Meta', $text_domain),
    'pages' => array( 'embassies' ),
    'fields' => array(


        array(
            'name' => __('Embassies Name', $text_domain),
            'id' => $prefix . 'embassies_name',
            'desc' => __("Enter your name.", $text_domain),
            'type'  => 'text',
            'std' => ''
        ),

        array(
            'name' => __('Embassies Number', $text_domain),
            'id' => $prefix . 'embassies_number',
            'desc' => __("Enter group personnes.", $text_domain),
            'type'  => 'text',
            'std' => ''
        ),

        array(
            'name' => __( 'Email', $text_domain ),
            'id'   => $prefix . 'embassies_email',
            'desc' => __("Enter email.", $text_domain),
            'type' => 'text',
        ),
        // SHOW PAGE TITLE
        array(
            'name' => __('Topbar title', $text_domain),
            'id' => $prefix . 'topbar_title',
            'clone' => false,
            'type'  => 'text',
            'std' => '',
        ),
    )
);
	
	/* Slider Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id'    => 'slider_meta_box',
		'title' => __('Page Slider Options', $text_domain),
		'pages' => array( 'page' ),
		'fields' => array(
			
			// SHOW COO SLIDER
			array(
				'name' => __('Show Coo Slider', $text_domain),
				'id'   => "{$prefix}posts_slider",
				'type' => 'checkbox',
				'desc' => __('Show the Coo Slider at the top of the page.', $text_domain),
				'std' => 0,
			),
			
			// COO SLIDER TYPE
			array(
				'name' => __('Coo Slider Type', $text_domain),
				'id'   => "{$prefix}posts_slider_type",
				'type' => 'select',
				'options' => array(
					'post'		=> __('Posts', $text_domain),
					'portfolio'	=> __('Portfolio', $text_domain),
					'hybrid'	=> __('Hybrid', $text_domain)
				),
				'multiple' => false,
				'std'  => 'post',
				'desc' => __('Choose the post type to display in the Coo Slider.', $text_domain),
			),
			
			// COO SLIDER CATEGORY
			array(
				'name' => __('Coo Slider category', $text_domain),
				'id'   => "{$prefix}posts_slider_category",
				'type' => 'select',
				'desc' => __('Select the category for which the Coo Slider should show posts from.', $text_domain),
				'options' => ct_get_category_list_key_array('category'),
				'std' => '',
			),
			
			// COO SLIDER PORTFOLIO CATEGORY
			array(
				'name' => __('Coo Slider portfolio category', $text_domain),
				'id'   => "{$prefix}posts_slider_portfolio_category",
				'type' => 'select',
				'desc' => __('Select the category for which the Coo Slider should show portfolio items from.', $text_domain),
				'options' => ct_get_category_list_key_array('portfolio-category'),
				'std' => '',
			),
			
			// COO SLIDER COUNT
			array(
				'name' => __('Coo Slider count', $text_domain),
				'id' => $prefix . 'posts_slider_count',
				'desc' => __("The number of posts to show in the Coo Slider.", $text_domain),
				'type'  => 'text',
				'std' => '5',
			),
			
			// SHOW FULL WIDTH REV SLIDER
			array(
				'name' => __('Revolution slider alias', $text_domain),
				'id' => $prefix . 'rev_slider_alias',
				'desc' => __("Enter the revolution slider alias for the slider that you want to show. NOTE: If you have the Coo Slider enabled above, then this will be ignored.", $text_domain),
				'type'  => 'text',
				'std' => '',
			),
			
			// SHOW FULL WIDTH REV SLIDER
			array(
				'name' => __('LayerSlider ID', $text_domain),
				'id' => $prefix . 'layerslider_id',
				'desc' => __("Enter the LayerSlider ID for the slider that you want to show. NOTE: If you have the Coo Slider enabled above, then this will be ignored.", $text_domain),
				'type'  => 'text',
				'std' => '',
			)
		)	
	);
	
		
	/* Page Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id'    => 'page_meta_box',
		'title' => __('Page Meta', $text_domain),
		'pages' => array( 'page' ),
		'fields' => array(
			
			// SIDEBAR OPTIONS SECTION
			array (
				'name' 	=> '',
				'title' => __('Sidebar Options', $text_domain),
			    'id' 	=> "{$prefix}heading_sidebar",
			    'type' 	=> 'section'
			),
			
			// SIDEBAR CONFIG
			array(
				'name' => __('Sidebar configuration', $text_domain),
				'id'   => "{$prefix}sidebar_config",
				'type' => 'select',
				'options' => array(
					'0-m-0'		=> __('No Sidebars', $text_domain),
                    '1-m-0'		=> __('Left Sidebar', $text_domain),
                    '0-m-1'		=> __('Right Sidebar', $text_domain),
                    '1-m-1'		=> __('Both Sidebars', $text_domain)
				),
				'multiple' => false,
				'std'  => $default_sidebar_config,
				'desc' => __('Choose the sidebar configuration for the detail page of this page.', $text_domain),
			),
			
			// LEFT SIDEBAR
			array (
				'name' 	=> __('Left Sidebar', $text_domain),
			    'id' 	=> "{$prefix}left_sidebar",
			    'type' 	=> 'sidebars',
			    'std' 	=> $default_left_sidebar
			),
			
			// RIGHT SIDEBAR
			array (
				'name' 	=> __('Right Sidebar', $text_domain),
			    'id' 	=> "{$prefix}right_sidebar",
			    'type' 	=> 'sidebars',
			    'std' 	=> $default_right_sidebar
			),

		)
	);
/* Page Meta Box
================================================== */
$meta_boxes[] = array(
    'id'    => 'destination_meta_box',
    'title' => __('Page Meta', $text_domain),
    'pages' => array( 'page' ),
    'fields' => array(

        // SIDEBAR OPTIONS destination
        array (
            'name' 	=> '',
            'title' => __('Destination Options', $text_domain),
            'id' 	=> "{$prefix}destination",
            'type' 	=> 'section'
        ),
        // BACKGROUND IMAGE
        array(
            'name'  => __('Image used as destination', $text_domain),
            'desc'  => __('The image that will be used as the post Destination, default size: height=980px,width:246px', $text_domain),
            'id'    => "{$prefix}destination_image",
            'type'  => 'image_advanced',
            'max_file_uploads' => 1
        )
    ));
	
	/* Gallery Meta Box
	================================================== */ 
	$meta_boxes[] = array(
		'id' => 'gallery_meta_box',
		'title' => __('Gallery Options', $text_domain),
		'pages' => array( 'galleries' ),
		'context' => 'normal',
		'fields' => array(
	
			// GALLERY IMAGES
			array(
				'name'             => __('Gallery Images', $text_domain),
				'desc'             => __('The images that will be used in the gallery.', $text_domain),
				'id'               => "{$prefix}gallery_images",
				'type'             => 'image_advanced',
				'max_file_uploads' => 200,
			)
		)
	);


	/********************* META BOX REGISTERING ***********************/
	
	/**
	 * Register meta boxes
	 *
	 * @return void
	 */
	function ct_register_meta_boxes()
	{
		global $meta_boxes;
	
		// Make sure there's no errors when the plugin is deactivated or during upgrade
		if ( class_exists( 'RW_Meta_Box' ) )
		{
			foreach ( $meta_boxes as $meta_box )
			{
				new RW_Meta_Box( $meta_box );
			}
		}
	}
	// Hook to 'admin_init' to make sure the meta box class is loaded before
	// (in case using the meta box class in another plugin)
	// This is also helpful for some conditionals like checking page template, categories, etc.
	add_action( 'admin_init', 'ct_register_meta_boxes' );

?>