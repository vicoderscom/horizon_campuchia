<?php

/*
*
*	Custom Advert Grid Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class ct_assistance_widget extends WP_Widget {

    function ct_assistance_widget() {
        $widget_ops = array( 'classname' => 'widget-assistance', 'description' => 'Styled Assistance of up to eight 125x125 adverts' );
        $control_ops = array( 'width' => 250, 'height' => 200, 'id_base' => 'assistance-widget' ); //default width = 250
        parent::__construct( 'assistance-widget', 'Cootheme Assistance Widget', $widget_ops, $control_ops );
    }

    function form($instance) {
        $defaults = array( 'title' => '', 'image' => '', 'phone' => '', 'skype1' => '', 'skype2' => '', 'email' => '');
        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>

        <p>
            <label><?php _e('Title', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label><?php _e('Image  URL', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo $instance['image']; ?>" class="widefat" type="text"/>
        </p>
        <p>
            <label><?php _e('Telephone : ', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" class="widefat" type="text"/>
        </p>
        <p>
            <label><?php _e('Skype : ', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'skype1' ); ?>" name="<?php echo $this->get_field_name( 'skype1' ); ?>" value="<?php echo $instance['skype1']; ?>" class="widefat" type="text"/>
            <input id="<?php echo $this->get_field_id( 'skype2' ); ?>" name="<?php echo $this->get_field_name( 'skype2' ); ?>" value="<?php echo $instance['skype2']; ?>" class="widefat" type="text"/>
        </p>
        <p>
            <label><?php _e('Email', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'email' ); ?>" name="<?php echo $this->get_field_name( 'email' ); ?>" value="<?php echo $instance['email']; ?>" class="widefat" type="text"/>
        </p>
    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['image'] = strip_tags( $new_instance['image'] );
        $instance['phone'] = strip_tags( $new_instance['phone'] );
        $instance['skype1'] = strip_tags( $new_instance['skype1'] );
        $instance['skype2'] = strip_tags( $new_instance['skype2'] );
        $instance['email'] = strip_tags( $new_instance['email'] );
        return $instance;
    }

    function widget($args, $instance) {

        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );
        $image = $instance['image'];
        $phone = $instance['phone'];
        $skype1 = $instance['skype1'];
        $skype2 = $instance['skype2'];
        $email = $instance['email'];
        $output = '';

        echo $before_widget;
        if ( $title ) echo $before_title . $title . $after_title;

        $output .= '<div class="sidebar-assistance">';

        $output .= '<div class="group-img-phone">';
            if ($image != "") {
                $output .= '<div>';
                    $output .= '<a href="'.$image.'" target="_blank">';
                    $output .= '<img src="'.$image.'" alt="advert" />';
                    $output .= '</a>';
                $output .= '</div>';
            }

            if ($phone != "") {
                $output .= '<div>';
                    $output .= '<span class="mobile-assistance">Mobile : '.$phone;
                    $output .= '</span>';
                $output .= '</div>';
            }
        $output .= '</div>';
        if (($skype1 || $skype2) != "") {
            $output .= '<div class="skype">';
                $output .= '<span>Skype : ';
                $output .= '</span>';
                $output .= '<div class="group-skype pull-right">';
                    $output .= '<a class="skype-image" href="skype:'.$skype1.'?chat" target="_blank"></i>';
                    $output .= '</a>';
                    $output .= '<a class="skype-image" href="skype:'.$skype2.'?chat" target="_blank"></i>';
                    $output .= '</a>';
                $output .= '</div>';
            $output .= '</div>';
        }

        if ($email != "") {
            $output .= '<div class="email-assistance">';
                $output .= '<span>Email : ';
                $output .= '</span>';
                $output .= '<a href="mailto:'.$email.'" target="_blank">'.$email;
                $output .= '</a>';
            $output .= '</div>';
        }



        $output .= '</div>';

        echo $output;

        echo $after_widget;

    }

}

add_action( 'widgets_init', 'ct_load_assistance_widget' );

function ct_load_assistance_widget() {
    register_widget('ct_assistance_widget');
}

?>
