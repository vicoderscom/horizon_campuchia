<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/
	
	// Register widget
	add_action( 'widgets_init', 'init_ct_sea_tour' );
	function init_ct_sea_tour() { return register_widget('ct_sea_tour'); }

	class ct_sea_tour extends WP_Widget {
		function ct_sea_tour() {
            parent::__construct( 'ct_sea_tour', $name = 'Cootheme SEA Tour' );
		}
	
		function widget( $args, $instance ) {

			global $post;
			extract($args);
            $cat_id="";
            if(is_tax('sea-category') ){
                $queried_object = get_queried_object();
                $cat_id = $queried_object->term_id;
            }

			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title		
			$number	 = $instance['number']; // Number of posts to show
			$post_categories	 = $instance['categories'];

            $name_categories = get_term_by('id', $post_categories, 'sea-category');
            $cat_name = $name_categories->name;
			echo $before_widget;
			
		    if ( $post_categories ) echo $before_title .$cat_name . $after_title;

            $args = array(
                'orderby'           => 'id',
                'order'             => 'DESC',
                'hide_empty'        => false,
                'parent'            => $post_categories
            );
            $categories = get_terms('sea-category',$args);
            if(count($categories) > 0)
            {?>
                <ul class="category-posts-list">
                    <?php  foreach($categories as $cate) {
                           $cat_ID=$cate->term_id;
                                $current='';
                            if($cat_ID == $cat_id){
                                $current='active';
                            }
                    ?>
                    <li class="cat-item <?php echo $current; ?>">
                        <a class="category-post-title" href="<?php echo get_category_link($cate); ?>" title="<?php echo ''; ?>"><?php echo $cate->name; ?></a>
                        <ul class="wg-post-items">
                            <?php
                            $args=array(
                                'post_type'         =>   'sea',
                                'posts_per_page'    =>   $number,
                                'orderby'           =>   'id',
                                'order'             =>   'DESC',
                                'tax_query'         =>   array(
                                    array(
                                        'taxonomy'  =>  'sea-category',
                                        'field'     =>  'id',
                                        'terms'     =>  $cat_ID,
                                    ),
                                ),
                            );
                            $child_terms = new WP_Query($args);
                            while ($child_terms->have_posts()) : $child_terms->the_post();?>
                                <li>
                                    <a  class="category-post-title" href="<?php the_permalink();?>">
                                       <?php the_title(); ?>
                                    </a>
                                </li>
                            <?php
                            endwhile;
                            ?>
                        </ul>
                    </li>

                    <?php } wp_reset_query();?>
                </ul>

                <?php echo $after_widget;
            }
		    }

        /* Widget control update */
        function update( $new_instance, $old_instance ) {
            $instance    = $old_instance;

            $instance['title']  = strip_tags( $new_instance['title'] );
            $instance['number'] = strip_tags( $new_instance['number'] );
            $instance['categories'] = strip_tags( $new_instance['categories'] );
            return $instance;
            }

		/* Widget settings */
		function form( $instance ) {	
		
			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
			    }
				
				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('categories'); ?>"><?php echo __( 'Choose Category:', 'coo-theme-admin'); ?></label>
                    <select  name="<?php echo $this->get_field_name('categories'); ?>">
                    <?php
                    $args = array(
                        'orderby'           => 'id',
                        'order'             => 'ASC',
                        'parent'            =>0,
                        'hide_empty'        => false,
                    );
                    $categories_option = get_terms('sea-category',$args);
                    foreach ($categories_option as $category) {
                        $option = '<option value="'.$category->term_id.'"'.selected( $categories, $category->term_id, false ).'>';
                        $option .= $category->name;
                        $option .= '</option>';
                        echo $option;
                    }?>
                    </select>
                </p>
		<?php 
		}
	
	}

?>