<?php

/*
*
*	Custom Posts Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

// Register widget
add_action( 'widgets_init', 'ct_destination_children' );
function ct_destination_children() { return register_widget('ct_destination_children'); }

class ct_destination_children extends WP_Widget {
    function ct_destination_children() {
        parent::__construct( 'ct_destination_children', $name = 'Cootheme  Destination Children' );
    }

    function widget( $args, $instance ) {

        global $post;
        extract($args);
        // Widget Options
        $number	 = $instance['number']; // Number of posts to show
        $title=get_queried_object()->name;
        $terms = get_the_terms( get_the_ID(), 'destination-category' );

        if ( ! empty( $terms ) ){
            $term = array_pop( $terms );
            $parent_term = ( $term->parent ? get_term( $term->parent, 'destination-category' ) : $term );
            $title=$parent_term->name;
            $child_terms = get_term_children( $parent_term->term_id, 'destination-category' );

        }
        else{
            $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
            $parent_term = get_term($term->parent, get_query_var('taxonomy') );
            $title=$parent_term->name;
            $child_terms = get_term_children( $parent_term->term_id, 'destination-category' );
        }

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title;?>


        <ul class="category-posts-list">
        <?php
            if ( ! empty( $child_terms ) ) :
                foreach ( $child_terms as $child_term_id ) :
                    $child_term = get_term_by( 'id', $child_term_id, 'destination-category' );
                    echo '<li class="cat-item"><a  class="category-post-title" href="' . get_term_link( $child_term, 'destination-category' ) . '">' . $child_term->name . "</a></li>\n";
                endforeach;
            endif;?>
        </ul>
        <?php
        echo $after_widget;
    }

    /* Widget control update */
    function update( $new_instance, $old_instance ) {
        $instance    = $old_instance;
        $instance['number'] = strip_tags( $new_instance['number'] );
        $instance['categories'] = strip_tags( $new_instance['categories'] );
        return $instance;
    }

    /* Widget settings */
    function form( $instance ) {

        // Set defaults if instance doesn't already exist
        if ( $instance ) {
            $number = $instance['number'];
            $categories=$instance['categories'];
        } else {
            $number = '5';
            $categories='';
        }

        // The widget form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of category to show:', 'coo-theme-admin'); ?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
        </p>

    <?php
    }

}

?>