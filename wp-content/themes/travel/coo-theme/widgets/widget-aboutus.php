<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/

	// Register widget
	add_action( 'widgets_init', 'init_ct_aboutus_posts' );
	function init_ct_aboutus_posts() { return register_widget('ct_aboutus_posts'); }

	class ct_aboutus_posts extends WP_Widget {
		function ct_aboutus_posts() {
			parent::__construct( 'ct_aboutus_posts', $name = 'Cootheme About Us' );
		}

		function widget( $args, $instance ) {

			global $post;
			extract($args);

			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title
			$number	 = $instance['number']; // Number of posts to show
            $title_link_extend = $instance['title_link_extend'];
            $url_link_extend = $instance['url_link_extend'];
            $title_link_extend_2 = $instance['title_link_extend_2'];
            $url_link_extend_2 = $instance['url_link_extend_2'];

			echo $before_widget;

            if ( $title ) echo $before_title . $title . $after_title;?>

            <ul class="category-posts-list">
            <?php
            $page_arr = array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-about-us.php'
            );

            $args = array(
                'orderby'           => 'id',
                'order'             => 'ASC',
                'hide_empty'        => false,
            );
            $aboutus_categories         =get_terms('aboutus-category',$args);

            $responsibility_category    =get_terms('responsibility-category',$args);

            $special_category           =get_terms('special-category',$args);

            $book_category              =get_terms('book-category',$args);

            $testimonials_category      =get_terms('testimonials-category',$args);

            $embassies_category         =get_terms('embassies-category',$args);

//            $aboutus_categories         = get_terms(
//                array('aboutus-category', 'responsibility-category','special-category','book-category','testimonials-category','embassies-category'),
//                $args
//            );

            $pages = get_posts($page_arr);

            foreach ($pages as $page) {
                $slug = get_post_field( 'post_name', $page->ID );
                // echo "<pre>";
                // var_dump($page->ID);die;
                $link_page = get_page_link( $page->ID );
                if($slug !== 'pourquoi-une-agence-locale' && (strcmp($link_page, $url_link_extend) !== 0)) :
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title;?></a>
                </li>
            <?php
                endif;
            }

            //=========== reverse between a page and a category =======
            foreach ($special_category as $category) {
                if($category->slug == 'pourquoi-nous') :
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
                endif;
            }

            foreach ($pages as $page) {
                $slug = get_post_field( 'post_name', $page->ID );
                if($slug == 'pourquoi-une-agence-locale') :
            ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title;?></a>
                </li>
            <?php
                endif;
            }

            //$hardcodePage = 7306;
            //$testpage = get_page_by_path('nos-bureaux', OBJECT, 'page');
            //$hardcodePageObj = get_post($hardcodePage);
            $hardcodePageObj = get_page_by_path('nos-bureaux', OBJECT, 'page');
            $hardcodePageUrl = get_page_link($hardcodePageObj->ID);
            ?>

            <li><a href="<?php echo $hardcodePageUrl; ?>"><?php echo $hardcodePageObj->post_title; ?></a></li>
            
            <?php


            //=====================================
            foreach ($special_category as $category) {
                if($category->slug !== 'pourquoi-nous') :
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
                endif;
            }
            foreach ($aboutus_categories as $category) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
            }
            foreach ($testimonials_category as $category) {
                if($category->slug === 'nos-voyageurs-parlent') :
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
                endif;
            }
            foreach ($embassies_category as $category) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
            }
            foreach ($book_category as $category) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
             <?php
            }
            foreach ($responsibility_category as $category) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
            }
            if(!empty($url_link_extend)):
            ?>
            <li class="cat-item">
                <a href="<?php echo $url_link_extend; ?>"><?php echo $title_link_extend; ?></a>
            </li>
			<?php
            endif;

            if(!empty($url_link_extend_2)):
            ?>
            <li class="cat-item">
                <a href="<?php echo $url_link_extend_2; ?>"><?php echo $title_link_extend_2; ?></a>
            </li>
            <?php
            endif;
			echo $after_widget;
		}

		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;

			$instance['title']  = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['categories'] = strip_tags( $new_instance['categories'] );
            $instance['title_link_extend'] = strip_tags( $new_instance['title_link_extend'] );
            $instance['url_link_extend'] = strip_tags( $new_instance['url_link_extend'] );
            $instance['title_link_extend_2'] = strip_tags( $new_instance['title_link_extend_2'] );
            $instance['url_link_extend_2'] = strip_tags( $new_instance['url_link_extend_2'] );
			return $instance;
		}

		/* Widget settings */
		function form( $instance ) {

			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
                    $title_link_extend = $instance['title_link_extend'];
                    $url_link_extend = $instance['url_link_extend'];
                    $title_link_extend_2 = $instance['title_link_extend_2'];
                    $url_link_extend_2 = $instance['url_link_extend_2'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
                    $title_link_extend = '';
                    $url_link_extend = '';
                    $title_link_extend_2 = '';
                    $url_link_extend_2 = '';
			    }

				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of category to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('title_link_extend'); ?>"><?php echo __( 'Title to show on left sidebar - link extend:', 'coo-theme-admin'); ?></label>
                    <input id="<?php echo $this->get_field_id('title_link_extend'); ?>" name="<?php echo $this->get_field_name('title_link_extend'); ?>" type="text" value="<?php echo $title_link_extend; ?>" class="widefat" />
                </p>
                <p>
                    <label for="<?php echo $this->get_field_id('url_link_extend'); ?>"><?php echo __( 'url for show on left sidebar - link extend:', 'coo-theme-admin'); ?></label>
                    <input id="<?php echo $this->get_field_id('url_link_extend'); ?>" name="<?php echo $this->get_field_name('url_link_extend'); ?>" type="text" value="<?php echo $url_link_extend; ?>" class="widefat" />
                </p>
                <hr>
                <p>
                    <label for="<?php echo $this->get_field_id('title_link_extend_2'); ?>"><?php echo __( 'Title to show on left sidebar 2 - link extend:', 'coo-theme-admin'); ?></label>
                    <input id="<?php echo $this->get_field_id('title_link_extend_2'); ?>" name="<?php echo $this->get_field_name('title_link_extend_2'); ?>" type="text" value="<?php echo $title_link_extend_2; ?>" class="widefat" />
                </p>
                <p>
                    <label for="<?php echo $this->get_field_id('url_link_extend_2'); ?>"><?php echo __( 'Url for show on left sidebar 2 - link extend:', 'coo-theme-admin'); ?></label>
                    <input id="<?php echo $this->get_field_id('url_link_extend_2'); ?>" name="<?php echo $this->get_field_name('url_link_extend_2'); ?>" type="text" value="<?php echo $url_link_extend_2; ?>" class="widefat" />
                </p>
		<?php
		}

	}

?>