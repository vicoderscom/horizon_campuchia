<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/

	// Register widget
	add_action( 'widgets_init', 'init_ct_page_links_posts' );
	function init_ct_page_links_posts() { return register_widget('ct_page_links_posts'); }

	class ct_page_links_posts extends WP_Widget {
		function ct_page_links_posts() {
			parent::__construct( 'ct_page_links_posts', $name = 'Cootheme Page Links' );
		}

		function widget( $args, $instance ) {

			global $post;
			extract($args);

			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title
			$number	 = $instance['number']; // Number of posts to show

			echo $before_widget;

            if ( $title ) echo $before_title . $title . $after_title;?>

            <ul class="category-posts-list">

            <?php
            $args = array(
                'orderby'           => 'id',
                'order'             => 'DESC',
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-contract.php',
            );
            $pages = get_posts($args);
            foreach ($pages as $page) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title;?></a>
                </li>
            <?php
            }?>

			<?php
			echo $after_widget;
		}

		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;

			$instance['title']  = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['categories'] = strip_tags( $new_instance['categories'] );
			return $instance;
		}

		/* Widget settings */
		function form( $instance ) {

			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
			    }

				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of category to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>

		<?php
		}

	}

?>