<?php

/*
*
*	Custom Phone Header Widget
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class ct_phone_header_widget extends WP_Widget {

    function ct_phone_header_widget() {
        $widget_ops = array( 'classname' => 'widget-phone-header', 'description' => 'Styled phone header of up to eight 125x125 adverts' );
        $control_ops = array( 'width' => 250, 'height' => 200, 'id_base' => 'phone-header-widget' ); //default width = 250
        parent::__construct( 'phone-header-widget', 'Cootheme Phone Header Widget', $widget_ops, $control_ops );
    }

    function form($instance) {
        $defaults = array( 'title' => '', 'phone' => '');
        $instance = wp_parse_args( (array) $instance, $defaults );

        ?>

        <p>
            <label><?php _e('Title', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
        </p>
        <p>
            <label><?php _e('Phone', 'coo-theme-admin');?>:</label>
            <input id="<?php echo $this->get_field_id( 'phone' ); ?>" name="<?php echo $this->get_field_name( 'phone' ); ?>" value="<?php echo $instance['phone']; ?>" class="widefat" type="text"/>
        </p>

    <?php
    }

    function update($new_instance, $old_instance) {
        $instance = $old_instance;

        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['phone'] = strip_tags( $new_instance['phone'] );

        return $instance;
    }

    function widget($args, $instance) {

        extract( $args );

        $title = apply_filters('widget_title', $instance['title'] );
        $phone = $instance['phone'];

        $output = '';

        echo $before_widget;

        if ($phone != "") {
            $output .= '<a href="tel:'.$phone.'">';
            $output .= '<i class="fa fa-phone" aria-hidden="true"></i>'.$phone;
            $output .= '</a>';
        }
        if ( $title ) echo $before_title.'('.$title.')'.$after_title;

        echo $output;

        echo $after_widget;

    }

}

add_action( 'widgets_init', 'ct_load_phone_header_widget' );

function ct_load_phone_header_widget() {
    register_widget('ct_phone_header_widget');
}

?>
