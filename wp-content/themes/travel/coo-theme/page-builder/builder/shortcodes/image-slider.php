<?php
/*
*
*	Coo Page Builder - Imapact Text Shortcode
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class CooPageBuilderShortcode_spb_image_slider extends CooPageBuilderShortcode {

    public  function content( $atts, $content = null ) {
        $img = $image = $color = $type = $target = $href = $border_top = $include_button = $button = $border_bottom = $title = $width = $position = $el_class = '';
        extract(shortcode_atts(array(
            'width' => '1/1',
            'image' =>$image,
            'href'  =>$href,
            'title' =>$title,
            'el_position' => '',
            'el_class' => ''
        ), $atts));

        $width = spb_translateColumnWidthToSpan($width);

        $title_link = '<a href="'.$href.'"><span>' . $title . '</span></a>';

        $img = spb_getImageBySize(array( 'attach_id' => preg_replace('/[^\d]/', '', $image), 'thumb_size' =>"img-slider"));
        $output = '';
        $output .= "\n\t".'<div class="spb_content_element spb_image_slider_widget">';
        $output .=$img['thumbnail'];
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}

SPBMap::map( 'spb_image_slider', array(
    "name"		=> __("Image Slider", "coo-page-builder"),
    "base"		=> "spb_image_slider",
    "class"		=> "spb_image_slider_widget",
    "icon"		=> "spb-icon-gallery",
    "params"	=> array(
        array(
            "type" => "textfield",
            "holder" => "h4",
            "class" => "image_slider",
            "heading" => __("Title", "coo-page-builder"),
            "param_name" => "title",
            "value" => "Title"
        ),
        array(
            "type" => "attach_image",
            "heading" => __("Image", "coo-page-builder"),
            "param_name" => "image",
            "value" => "",
            "description" => ""
        ),
        array(
            "type" => "textfield",
            "heading" => __("Add link", "coo-page-builder"),
            "param_name" => "href",
            "value" => "#",
            "description" => __("If you would  to link to a URL, then enter it here.", "coo-page-builder")
        )

    )
) );
?>