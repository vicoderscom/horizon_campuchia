<?php

class CooPageBuilderShortcode_booking extends CooPageBuilderShortcode {

    protected function content($atts, $content = null) {

        $options = get_option('ct_coo_options');

        $item_limit=$title = $category = $item_class = $excerpt_length = $width = $offset = $el_class = $output = $filter = $items = $el_position = $item_count = '';

        extract(shortcode_atts(array(
            'title'         =>'Title',
            "category"		=> 'all',
            'el_position'   => '',
            'width'         => '1/1',
            'el_class'      => '',
            'on_off'        => 'on'
        ), $atts));

        $args = array(
            'orderby'           => 'id',
            'order'             => 'ASC',
            'hide_empty'        => false,
            'parent'            => 0
        );

        $idObj = get_category_by_slug($category);
        $cat_id = $idObj->term_id;

        $args = array(
            'orderby'           => 'id',
            'order'             => 'ASC',
            'hide_empty'        => false,
            'parent'            => $cat_id
        );
        $categories = get_categories($args);


        $width = spb_translateColumnWidthToSpan($width);
        $el_class = $this->getExtraClass($el_class);
        
        $on_off = $this->getExtraClass($on_off);
        
        $output = '';
        $output .= "\n\t".'<div class="booking_widget spb_content_element '.$width.' '.$on_off.' ">';

        $output .='<h2 class="text-des">'.$title.'</h2>';

        $output .= '<ul class="row '.$el_class.' content-page-category clearfix">';
            if(count($categories) > 0)
            {
                foreach($categories as $cate) {
                    $attachment_id   = get_option('categoryimage_'.$cate->term_id);
                    $image_src = wp_get_attachment_image_src($attachment_id, 'img-introduce')[0];

                    $output .= '<li class="item-destination col-md-4">';
                    $output .= '<div class="item-destination-inner">';
                    $output .= '<div class="item-category-inner clearfix">';
                    $output .= '<h3 class="no-margin title-category"><a class="title-category" href="'.get_category_link($cate).'">'.$cate->name.'</a></h3>';
                    $output .= '<a href="'.get_category_link($cate).'">';
                    $output .= '<img itemprop="image" class="img-responsive" src="'.$image_src.'" alt="'.$cate->name.'">';
                    $output .= '</a>';
                    $output .= '</div>';
                    $output .= '<div class="description">';
                    $output .=  ct_custom_excerpt($cate->description,20);
                    $output .= '</div>';
                    $output .= '<a href="'.get_category_link($cate).'" class="icon-read-more">[ + ]</a>';
                    $output .= '</div>';
                    $output .= '</li>';
                }
            }
        $output .= '</ul>';
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;

        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);

        global $ct_include_carousel, $ct_include_isotope;
        $ct_include_carousel = true;
        $ct_include_isotope = true;

        return $output;

    }
}

SPBMap::map( 'booking', array(
    "name"		=> __("Booking", "coo-page-builder"),
    "base"		=> "booking",
    "class"		=> "spb_booking ",
    "icon"      => "spb-icon-posts-carousel",
    "params"	=> array(

        array(
            "type" => "textfield",
            "holder" => "h3",
            "heading" => __("Title categiry", "coo-page-builder"),
            "param_name" => "title",
            "value" => "",
            "description" => __("Heading text. Leave it empty if not needed.", "coo-page-builder")
        ),
        array(
            "type" => "select-multiple",
            "heading" => __("choose  category", "coo-page-builder"),
            "param_name" => "category",
            "value" => ct_get_category_booking(),
            "description" => __("Choose the category for Tour.", "coo-page-builder")
        ),

        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "coo-page-builder"),
            "param_name" => "el_class",
            "value" => "",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
        ),

        array(
            "type" => "select-multiple",
            "heading" => __("On / Off option", "coo-page-builder"),
            "param_name" => "on_off",
            "value" => array(
                "on" => "on",
                "off" => "off",
            ),
            "description" => __("On / Off option", "coo-page-builder")
        )
    )
) );




?>