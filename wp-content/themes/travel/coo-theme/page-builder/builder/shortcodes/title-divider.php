<?php
/*
*
*	Coo Page Builder - Imapact Text Shortcode
*	------------------------------------------------
*	Cootheme
* 	http://www.cootheme.com
*
*/

class CooPageBuilderShortcode_spb_title_divider extends CooPageBuilderShortcode {

    public  function content( $atts, $content = null ) {
        $display = $title = $width = $position = $el_class = '';
        extract(shortcode_atts(array(
            'width' => '1/1',
            'title' =>$title,
            'position' => 'cta_align_center',
            'display'=>'',
            'el_position' => '',
            'el_class' => ''
        ), $atts));

        $width = spb_translateColumnWidthToSpan($width);
        $display = $this->getExtraClass($display);
        $el_class = $this->getExtraClass($el_class);
        $output = '';
        $output .= "\n\t".'<div class="spb_title_divider_widget spb_content_element '.$width.'">';

        if ($position == "cta_align_left") {
            $output .='<div class="content align_left">';
            $output .= '<h2 class="title_block '.$el_class.'">'.$title.'</h2>';
            $output .='</div>'."\n";
        } elseif ($position == "cta_align_right") {
            $output .= '<div class="content align_right">';
            $output .= '<h2 class="title_block '.$el_class.' '.$display.'">'.$title.'</h2>';
            $output .='</div>'."\n";
        }
        else{
            $output .= '<div class="content align_center">';
            $output .= '<h2 class="title_block '.$el_class.' '.$display.'">'.$title.'</h2>';
            $output .='</div>'."\n";
        }
        $output .= "\n\t".'</div> '.$this->endBlockComment($width);
        $output = $this->startRow($el_position) . $output . $this->endRow($el_position);
        return $output;
    }
}
SPBMap::map( 'spb_title_divider', array(
    "name"		=> __("Title Block", "coo-page-builder"),
    "base"		=> "spb_title_divider",
    "class"		=> "spb_title_divider_widget",
    "icon"		=> "spb-icon-gallery",
    "params"	=> array(

        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title", "coo-page-builder"),
            "param_name" => "title",
            "value" => "title block",
            "description" => __("Title blog , then enter it here.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Description position", "coo-page-builder"),
            "param_name" => "position",
            "value" => array(__("Align right", "coo-page-builder") => "cta_align_right", __("Align left", "coo-page-builder") => "cta_align_left", __("Align center", "coo-page-builder") => "cta_align_center"),
            "description" => __("Select description alignment.", "coo-page-builder")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Display Title", "coo-page-builder"),
            "param_name" => "display",
            "value" => array(__("Yes", "coo-page-builder") => "display_block", __("No", "coo-page-builder") => "display_none"),
            "description" => __("Show title", "coo-page-builder")
        ),
          array(
              "type" => "textfield",
              "heading" => __("Extra class name", "coo-page-builder"),
              "param_name" => "el_class",
              "value" => "",
              "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "coo-page-builder")
          )
    )
) );
?>