<?php

	/*
	*
	*	Custom Posts Widget
	*	------------------------------------------------
	*	Cootheme
	* 	http://www.cootheme.com
	*
	*/

	// Register widget
	add_action( 'widgets_init', 'init_ct_levietnam_posts' );
	function init_ct_levietnam_posts() { return register_widget('ct_levietnam_posts'); }

	class ct_levietnam_posts extends WP_Widget {
		function ct_levietnam_posts() {
			parent::__construct( 'ct_levietnam_posts', $name = 'Cootheme Culture Posts' );
		}

		function widget( $args, $instance ) {

			global $post;
			extract($args);

			// Widget Options
			$title 	 = apply_filters('widget_title', $instance['title'] ); // Title
			$number	 = $instance['number']; // Number of posts to show

			echo $before_widget;

            if ( $title ) echo $before_title . $title . $after_title;?>

            <ul class=category-posts-list">
            <?php
            $post_categories=get_terms('levietnam-category' );
            foreach ($post_categories as $category) {
                ?>
                <li class="cat-item">
                    <a class="category-post-title" href="<?php echo get_term_link($category->slug, 'levietnam-category'); ?>" title="<?php echo $category->name; ?>"><?php echo $category->name; ?></a>
                </li>
            <?php
            }?>
			<?php

			echo $after_widget;
		}

		/* Widget control update */
		function update( $new_instance, $old_instance ) {
			$instance    = $old_instance;

			$instance['title']  = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['categories'] = strip_tags( $new_instance['categories'] );
			return $instance;
		}

		/* Widget settings */
		function form( $instance ) {

			    // Set defaults if instance doesn't already exist
			    if ( $instance ) {
					$title  = $instance['title'];
			        $number = $instance['number'];
                    $categories=$instance['categories'];
			    } else {
				    // Defaults
					$title  = '';
			        $number = '5';
                    $categories='';
			    }

				// The widget form
				?>
				<p>
					<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'coo-theme-admin' ); ?></label>
					<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:', 'coo-theme-admin'); ?></label>
					<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
				</p>
                <p>
                    <label for="<?php echo $this->get_field_id('categories'); ?>"><?php echo __( 'Choose Category:', 'coo-theme-admin'); ?></label>
                    <select  name="<?php echo $this->get_field_name('categories'); ?>">
                    <?php
                    $args = array(
                        'orderby' => 'name',
                    );
                    $post_categories=get_terms('levietnam-category' );
                    foreach ($post_categories as $category) {
                        $option = '<option value="'.$category->term_id.'">';
                        $option .= $category->name;
                        $option .= '</option>';
                        echo $option;
                    }?>
                    </select>
                </p>
		<?php
		}

	}

?>