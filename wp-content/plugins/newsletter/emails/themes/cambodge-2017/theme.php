<?php
if (!defined('ABSPATH'))
    exit;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<div class="content" style="max-width: 750px; margin: 0 auto; background: #ebebeb;">
<div class="body-conten">
<div class="section1"><img style="max-width: 100%; width: 100%;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/logo-ban.png" alt="" /></div>
<div class="section2"><img style="max-width: 100%; width: 100%;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/banner1.png" alt="" /></div>
<div class="section3" style="font-family: georgia, Arial, Helvetica, sans-serif; font-style: italic; max-width: 551px; margin: 0 auto; background: white; padding: 15px; margin-top: -51px; z-index: 1;">
<h1 style="font-size: 16px;">Cher monsieur {name},</h1>
<p style="font-size: 14px;">Toute l'équipe de HORIZON VIETNAM TRAVEL se joint à moi pour vous remercier de la confiance que vous nous avez accordée durant ces derniers temps</p>
<p style="font-size: 14px;">En cette période des fê tes, nous vous transmettons nos meilleurs væux de santé, joie, bonheur et prospérité à vous et votre famille pour la nouvelle année 2017, Au plaisir de vous revoir !</p>
<h2 style="font-size: 15px; font-weight: normal; text-align: center;">TA BAU et l`équipe de HORIZON VIETNAM TRAVEL</h2>
</div>
<div class="bg" style="background: #fff url('http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/bg.png'); background-repeat: no-repeat; background-position: 0% 111%; max-width: 581px; margin: 0 auto;">
<div class="section4" style="font-family: utm_androgyneregular; max-width: 100%; margin: 0 auto; z-index: 1;"><img style="width: 100%; height: auto;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/khung.png" alt="" /></div>
<div class="section5" style="font-family: utm_androgyneregular; max-width: 100%px;">
<div class="trai" style="width: 50%; float: left;"><img style="max-width: 100%; width: 100%;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/trai.png" alt="" /></div>
<div class="phai"><a href="http://cambodge.horizon-vietnamvoyage.com/"><img style="max-width: 100%; width: auto;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/phai.png" alt="" /></a></div>
</div>
<img src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/brand.png" alt="" /></div>
<div class="footer" style="background: #117700; text-align: center; color: #fff; padding: 10px;">
<div class="country">
<ul>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/vn.png" alt="" /><span>Viet Nam</span></li>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/cam.png" alt="" /><span>Campodge</span></li>
<li style="list-style: none; display: inline-block; margin-right: 30px;"><img style="vertical-align: middle; margin-right: 3px;" src="http://horizon-vietnamvoyage.com/wp-content/uploads/2016/12/lao.png" alt="" /><span>Laos</span></li>
</ul>
</div>
<div class="contact">
<p style="font-size: 15px; margin: 5px;">© Droit d'auteur par HORIZON VIETNAM TRAVEL</p>
<p style="font-size: 15px; margin: 5px;">Licence d` état de voyage International - N° 01-387/TCDL- GP LHQT</p>
<p style="font-size: 15px; margin: 5px;">Agrée par la Direction Générale du Tourisme du Vietnam</p>
<p style="font-size: 15px; margin: 5px;">Si vous avez des difficultés pour visualiser ce message <a style="color: yellow;" href="http://horizon-vietnamvoyage.com/{email_url}">cliquez ici</a></p>
<p><em><font face="helvetica">Pour vous désabonner de la newsletter <a style="color: yellow;" href="http://horizon-vietnamvoyage.com/{unsubscription_url}">cliquez ici</a></font></em></p>
</div>
</div>
</div>
</div>
</html>